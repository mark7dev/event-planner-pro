import React from 'react';
import { graphql } from 'gatsby';
import Image from 'gatsby-image';
import Layout from '../components/layout';
import styled from '@emotion/styled';

export const query = graphql`
  query($slug: String!) {
    allDatoCmsBlog(filter: {slug: {eq: $slug} }) {
      nodes {
        title
        preview
        author
        role
        content
        id
        image {
          fluid(maxWidth: 1200) {
            ...GatsbyDatoCmsFluid
          }
        }
      }
    }
  }
`;

const BlogTitle = styled.h1`
  text-align: center;
  margin-top: 4rem;
`;

const BlogPreview = styled.h3`
  text-align: center;
  margin-top: 2rem;
`;

const BlogContainer = styled.main`
  margin: 0 auto;
  max-width: 1200px;
  width: 95%;
  padding: 40px;

  @media (min-width: 768px) {
    width: 90%;
  }

  @media (min-width: 992px) {
    width: 80%;
  }

  @media (min-width: 1200px) {
    width: 70%;
  }

  @media (min-width: 1920px) {
    width: 60%;
  }
`;

const AuthorContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 40px;
  flex: 1;

  @media (max-width: 768px) {
    align-items: center;
    text-align: center;
  }

  @media (min-width: 768px) {
    flex-direction: row;
    justify-content: center;
  }

  @media (min-width: 992px) {
    flex-direction: row;
    justify-content: center;
  }

  @media (min-width: 1200px) {
    flex-direction: row;
    justify-content: center;
  }

  @media (min-width: 1920px) {
    flex-direction: row;
    justify-content: center;
  }
`;

const AuthorImage = styled.div`
  width: 100px;
  height: 100px;
  border-radius: 50%;
  overflow: hidden;
  margin: 20px auto;

  @media (min-width: 768px) {
    margin: 0px 20px;
  }

  @media (min-width: 992px) {
    margin: 0px 20px;
  }

  @media (min-width: 1200px) {
    margin: 0px 20px;
  }

  @media (min-width: 1920px) {
    margin: 0px 20px;
  }
`;

const AuthorName = styled.p`
  font-size: 20px;
  font-weight: 600;
  line-height: 26px;
  text-align: center;
  line-height: 0px;

  @media (min-width: 768px) {
    margin: 0px 20px;
  }

  @media (min-width: 992px) {
    margin: 0px 20px;
  }

  @media (min-width: 1200px) {
    margin: 0px 20px;
  }

  @media (min-width: 1920px) {
    margin: 0px 20px;
  }
`;

const AuthorRole = styled.p`
  font-size: 16px;
  line-height: 26px;
  text-align: center;
  line-height: 0px;

  @media (min-width: 768px) {
    margin: 0px 20px;
  }

  @media (min-width: 992px) {
    margin: 0px 20px;
  }

  @media (min-width: 1200px) {
    margin: 0px 20px;
  }

  @media (min-width: 1920px) {
    margin: 0px 20px;
  }
`;

const BlogContent = styled.p`
  font-size: 16px;
  line-height: 26px;
  max-width: 100%;
  margin: 0 auto;
  margin-top: 2rem;
  margin-bottom: 2rem;

  @media (min-width: 768px) {
    margin: 0px 20px;
  }

  @media (min-width: 992px) {
    margin: 0px 20px;
  }

  @media (min-width: 1200px) {
    margin: 0px 20px;
  }

  @media (min-width: 1920px) {
    margin: 0px 20px;
  }
`;

const BlogText = ({ data: { allDatoCmsBlog: { nodes } } }) => {
  
  const { title, preview, author, role, content, image } = nodes[0];

  return (
    <Layout>
      <BlogContainer>
        <BlogTitle>{title}</BlogTitle>
        <BlogPreview>{preview}</BlogPreview>
        <AuthorContainer>
          <AuthorImage>
            <Image fluid={image.fluid} />
          </AuthorImage>
          <AuthorName>{author}</AuthorName>
          <AuthorRole>{role}</AuthorRole>
        </AuthorContainer>
        <BlogContent>{content}</BlogContent>
      </BlogContainer>
    </Layout>
  )
}

export const Head = ({ data: { allDatoCmsBlog: { nodes } } }) => {
  const { title, preview, author, slug } = nodes[0];

  return (
    <>
        <title>{title} | EventPlanner Pro</title>
        <meta name="description" content={preview} />
        <meta name="keywords" content="blog, event planning, keywords, related, to, the, post" />
        <meta name="author" content={author} />
        <meta property="og:title" content={`${title} | EventPlanner Pro`} />
        <meta property="og:description" content={preview} />
        <meta property="og:image" content="https://www.example.com/blog-post-image.png" />
        <meta property="og:url" content={`https://astounding-sprinkles-1b5be0.netlify.app/blog/${slug}/`} />
        <meta property="og:type" content="article" />
        <meta property="og:site_name" content="EventPlanner Pro" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="Blog | Event PLanner Pro" />
        <meta name="twitter:description" content={preview} />
        <meta name="twitter:image" content="https://www.example.com/blog-post-image.png" />
    </>
  )
}

export default BlogText;
