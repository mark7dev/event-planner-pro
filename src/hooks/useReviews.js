import { graphql, useStaticQuery } from "gatsby";

const useReviews = () => {
    const data = useStaticQuery(graphql`
        query {
            allDatoCmsReview {
                nodes {
                    content
                    author
                    role
                    slug
                    rating
                    id
                    image {
                        fluid(maxWidth: 1200) {
                            ...GatsbyDatoCmsFluid
                        }
                    }
                }
            }
        }
    `);


    return data.allDatoCmsReview.nodes.map(review => ({
        content: review.content,
        author: review.author,
        role: review.role,
        slug: review.slug,
        rating: review.rating,
        id: review.id,
        image: review.image,
    }));

};

export default useReviews;