import { graphql, useStaticQuery } from "gatsby";

const usePartners = () => {
    const data = useStaticQuery(graphql`
        query {
            allDatoCmsPartner {
                nodes {
                    name
                    role
                    id
                    image {
                        fluid(maxWidth: 1200) {
                            ...GatsbyDatoCmsFluid
                        }
                    }
                }
            }
        }
    `);


    return data.allDatoCmsPartner.nodes.map(partner => ({
        name: partner.name,
        role: partner.role,
        id: partner.id,
        image: partner.image,
    }));

};

export default usePartners;