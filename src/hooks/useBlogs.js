import { graphql, useStaticQuery } from "gatsby";

const useBlogs = () => {
    const data = useStaticQuery(graphql`
        query {
            allDatoCmsBlog {
                nodes {
                    title
                    preview
                    author
                    role
                    content
                    slug
                    id
                    image {
                        fluid(maxWidth: 1200) {
                            ...GatsbyDatoCmsFluid
                        }
                    }
                }
            }
        }
    `);

    return data.allDatoCmsBlog.nodes.map(blog => ({
        title: blog.title,
        preview: blog.preview,
        author: blog.author,
        role: blog.role,
        content: blog.content,
        image: blog.image,
        slug: blog.slug,
        id: blog.id
    }));

};

export default useBlogs;