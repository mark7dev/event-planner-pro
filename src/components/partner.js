import React from 'react';
import styled from '@emotion/styled';
import { css } from '@emotion/react';
import Image from 'gatsby-image';

const Name = styled.h6`
    font-size: 20px;
    font-weight: 600;
    line-height: 26px;
    text-align: center;
    line-height: 0px;
    color: #2B2D42;
`;

const Role = styled.p`
    font-size: 16px;
    line-height: 26px;
    text-align: center;
    line-height: 0px;
    color: #8D99AE;
`;

const Partner = ({partner}) => {

  const { name, role, image } = partner;

  return (
    <div
      css={css`
        width: 200px;
        height: 300px;
        padding: 20px;
        margin: 20px;
        background-color: #FFFFFF;
        border-radius: 10px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
      `}
    >
      <div 
        css={css`
          width: 100px;
          height: 100px;
          border-radius: 50%;
          overflow: hidden;
          margin: 20px auto;
        `}
      >
        <Image fluid={image.fluid}/>
      </div>
      <Name>{name}</Name>
      <Role>{role}</Role>
    </div>
  )
}

export default Partner;
