import React from 'react';
import styled from '@emotion/styled';
import { Link } from 'gatsby';

const GoToBlog = styled(Link)`
    text-decoration: none;
    color: #FFFFFF;
    font-size: 16px;
    font-weight: 600;
    line-height: 19px;
    margin-top: 20px;
    background-color: #2B2D42;
    padding: 10px 20px;
    border-radius: 12px;
    transition: all 0.3s ease-in-out;
    &:hover {
        background-color: #F8F32B;
        color: #000000;
    }
`;

const Container = styled.div`
    top: 1260px;
    left: 200px;
    width: 310px;
    height: 450px;
    background-color: #f0f0f0;
    border-radius: 12px;
    margin: 20px;
    padding: 20px;
`;

const Title = styled.h1`
    font-size: 24px;
    line-height: 31px;
`;

const Preview = styled.p`
    font-size: 20px;
    line-height: 24px;
`;

const Author = styled.p`
    font-size: 16px;
    font-weight: 600;
    line-height: 19px;
`;

const Role = styled.p`
    font-size: 14px;
    line-height: 16px;
`;

const CardBlog = ({blog}) => {

    const { title, preview, author, role, slug } = blog;

    return (
        <Container>
            <Title>{title}</Title>
            <Preview>{preview}</Preview>
            <Author>{author}</Author>
            <Role>{role}</Role>
            <GoToBlog to={slug}>Go to blog</GoToBlog>
        </Container>
    )
}

export default CardBlog;
