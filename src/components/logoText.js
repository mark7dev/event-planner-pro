import React from 'react';
import styled from '@emotion/styled';

const TextStyled = styled.h2`
  font-family: 'Montserrat', sans-serif;
  color: #8d99ae;
  text-shadow: 2px 2px #f5af19;
  margin: 0;
  letter-spacing: 0.1em;
`;

const LogoText = () => {
  return (
    <TextStyled>EventPlanner Pro</TextStyled>
  );
}

export default LogoText;
