import React from 'react';
import { Link } from 'gatsby';
import styled from '@emotion/styled';

const Navigation = styled.nav`
    display: flex;
    justify-content: flex-end;
    align-items: center;
`;

const NavLink = styled(Link)`
    font-size: 1.2rem; /* Updated font-size */
    font-weight: 700;
    line-height: 1rem;
    font-family: 'PT Sans', sans-serif;
    text-decoration: none;
    padding: 1rem;
    margin-right: 1rem;
    color: #FFFFFF;

    &:last-of-type {
        margin-right: 0;
    }

    &.current-page {
        border-bottom: 2px solid #F8F32B;
    }

    @media (max-width: 768px) {
        font-size: 1.2rem;
        padding: 0.5rem;
        margin-right: 0.5rem;
    }
`;

const NavBar = () => {
    return (
        <Navigation>
            <NavLink
                to={'/'}
                activeClassName='current-page'
            >Home</NavLink>
            <NavLink
                to={'/about'}
                activeClassName='current-page'
            >About</NavLink>
            <NavLink
                to={'/pricing'}
                activeClassName='current-page'
            >Pricing</NavLink>
            <NavLink
                to={'/blog'}
                activeClassName='current-page'
            >Blog</NavLink>
        </Navigation>
    )
}

export default NavBar;
