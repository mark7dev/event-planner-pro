import React from 'react';
import { Link } from 'gatsby';
import { css } from '@emotion/react';
import styled from '@emotion/styled';
import LogoText from './logoText';
import NavBar from './navBar';

const HomeLink = styled(Link)`
    color: #FFFFFF;
    text-decoration: none;
    font-size: 1.5rem;
`;

const Header = () => {
    return (
        <header
            css={css`
                background-color: #2B2D42;
                color: #FFFFFF;
                padding: 1rem;
            `}
        >
            <div
                css={css`
                    margin: 0 auto;
                    max-width: 1200px;
                    display: flex;
                    align-items: center;
                    justify-content: space-between;

                    @media (max-width: 768px) {
                        flex-direction: column;
                        align-items: flex-start;
                    }
                `}
            >
                <HomeLink to='/'>
                    <LogoText />
                </HomeLink>
                <NavBar />
            </div>
        </header>
    );
};

export default Header;
