import React from 'react';
import styled from '@emotion/styled';
import Image from 'gatsby-image';

const CardContainer = styled.div`
    outline: 2px solid #2B2D42;;
    width: 310px;
    height: 350px;
    background-color: #f0f0f0;
    border-radius: 12px;
    padding: 20px;
    margin: 20px;
    box-shadow: 0 0 10px rgba(0,0,0,0.2);
    transition: all 0.3s ease-in-out;
    color: black;
    &:hover {
        transform: scale(1.05);
        box-shadow: 0 0 20px rgba(0,0,0,0.2);
    }
`;

const ReviewContent = styled.p`
    font-size: 14px;
    font-weight: 600;
    margin-top: 20px;
    height: 80px;
`;

const ReviewerData = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;

const ReviewerImage = styled.div`
    width: 100px;
    height: 100px;
    border-radius: 50%;
    overflow: hidden;
    margin: 20px auto;
`;

const ReviewerInfo = styled.div`
    flex: 1;
    margin-left: 20px;
`;

const Name = styled.h6`
    color: #2B2D42;
`;

const Role = styled.p`
    font-size: 12px;
    font-weight: 600;
    color: #8D99AE;
    line-height: 0rem;
`;

const CardReview = ({review}) => {

    const { author, content, image, rating, role } = review;

    const starsGenerator  = (rating) => {
        const elements = new Array(rating).fill('star');
        return elements
    }

    return (
        <CardContainer>
            {starsGenerator(rating).map((star, index) => (
                <span key={index} className="fa fa-star" style={{color: '#8D99AE'}}></span>
            ))}
            <ReviewContent>{content}</ReviewContent>
            <ReviewerData>   
                <ReviewerImage>
                    <Image fluid={image.fluid}/>
                </ReviewerImage>
                <ReviewerInfo>
                    <Name>{author}</Name>
                    <Role>{role}</Role>
                </ReviewerInfo>
                
            </ReviewerData>
        </CardContainer>
    )
}

export default CardReview;
