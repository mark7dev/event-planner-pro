import React from 'react';
import { FaFacebook, FaTwitter, FaInstagram } from 'react-icons/fa';
import styled from '@emotion/styled';
import { css } from '@emotion/react';
import LogoText from './logoText';

const FooterContainer = styled.footer`
    background-color: #2B2D42;
    color: #FFFFFF;
    padding: 1rem;
`;

const FooterContent = styled.div`
    margin: 0 auto;
    max-width: 1200px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;

    @media (max-width: 768px) {
        flex-direction: column;
        align-items: center;
        align-content: center;
        justify-content: center;
    }
`;

const SocialLinks = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 20px;

    @media (max-width: 768px) {
        margin-top: 20px;
        margin-bottom: 0;
    }
`;

const SocialLink = styled.a`
    color: #FFFFFF;
    text-decoration: none;
    font-size: 1.5rem;
    margin-right: 20px;

    &:hover {
        color: #F8F32B;
    }

    @media (max-width: 768px) {
        margin-right: 10px;
    }
`;

const ContactInfo = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    margin-top: 0;

    @media (max-width: 768px) {
        margin-top: 20px;
        align-items: center;
    }
`;

const ContactTitle = styled.h3`
    color: #FFFFFF;
    font-size: 1.5rem;
    margin-bottom: 1rem;
`;

const ContactItem = styled.p`
    color: #FFFFFF;
    font-size: 1.2rem;
    margin: 0.5rem 0;
`;

const InfoFooter = styled.p`
    font-weight: 700;
    line-height: 0rem;
    font-family: 'PT Sans', sans-serif;
    margin-left: 1rem;
    color: #8D99AE;

    @media (max-width: 768px) {
        margin-top: 20px;
        text-align: center;
    }
`;

const Footer = () => {
    const year = new Date().getFullYear();

    return (
        <FooterContainer>
            <FooterContent>
                <LogoText />
                <SocialLinks>
                    <SocialLink href='#'><FaFacebook /></SocialLink>
                    <SocialLink href='#'><FaTwitter /></SocialLink>
                    <SocialLink href='#'><FaInstagram /></SocialLink>
                </SocialLinks>
                <ContactInfo>
                    <ContactTitle>Contact information</ContactTitle>
                    <ContactItem style={{ color: '#8D99AE' }}>Phone Number: +1 (123) 456-7890</ContactItem>
                    <ContactItem style={{ color: '#8D99AE' }}>Email Address: hello@eventplannerpro.com</ContactItem>
                </ContactInfo>
            </FooterContent>
            <InfoFooter
                css={css`
                    margin-top: 2rem;
                    text-align: center;
                    color: #8D99AE;
                `}
            >All rights reserved {year} &copy; EventPlanner Pro</InfoFooter>
        </FooterContainer>
    );
};

export default Footer;
