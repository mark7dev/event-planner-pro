import * as React from "react";

import Layout from "../components/layout";
import CardReview from "../components/cardReview";
import useReviews from "../hooks/useReviews";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { StaticImage } from "gatsby-plugin-image";
import Seo from "../components/seo";

const Section = styled.section`
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: 0 20px;
  margin: 0 auto;
  width: 100%;
  min-height: 100vh;
  background-color: #2B2D42;
  color: #FFFFFF;

  @media (max-width: 768px) {
    flex-direction: column;
    height: auto;
  }
`;

const ContainerL = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  text-align: end;
  padding: 40px;
  flex: 1;

  @media (max-width: 768px) {
    align-items: center;
    text-align: center;
    padding: 20px;
  }
`;

const ContainerR = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 50%;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

const MainText = styled.h1`
  font-size: 48px;
  font-weight: 700;
  line-height: 62px;
  margin-bottom: 40px;
`;

const SecondaryText = styled.p`
  font-size: 24px;
  font-weight: 400;
  line-height: 32px;
  margin-bottom: 20px;
  padding: 0 20px;
`;

const ReviewsSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #FFFFFF;
  color: #000000;
  padding: 80px 0;

  @media (max-width: 768px) {
    padding: 40px 0;
  }
`;

const IndexPage = () => {

  const reviews = useReviews();

  return (
    <Layout>
      <Section>
        <ContainerL>
        <MainText>Simplified Event Planning</MainText>
          <SecondaryText>Online Registration and Ticketing</SecondaryText>
          <SecondaryText>Attendee Communication Hub</SecondaryText>
          <SecondaryText>Event Analytics and Reporting</SecondaryText>
        </ContainerL>
        <ContainerR>
          <StaticImage
            src="../images/image1.png"
            alt="Event Planner"
            placeholder="blurred"
            layout="constrained"
            width={500}
            quality={100}
            formats={["auto", "webp", "avif"]}
            css={css`
              border-radius: 10px;
            `}
          />
        </ContainerR>
      </Section>
      <ReviewsSection>
        <MainText>What Our Customers Are Saying</MainText>
        <SecondaryText>Join over 1 million event professionals from around the world who trust our product. Read their reviews below!</SecondaryText>
        <div
          css={css`
            display: flex;
            justify-content: space-around;
            align-items: center;
            flex-wrap: wrap;
            padding: 0 20px;
            margin: 40px 0px;
            width: 80%;

            @media (max-width: 1370px) {
              width: 100%;
            }
          `}
        >
          {reviews.map(review => (
            <CardReview 
              key={review.id}
              review={review}
            />
          ))}
        </div>
      </ReviewsSection>
    </Layout>
  )
  
}

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="EventPlanner Pro" />

export default IndexPage
