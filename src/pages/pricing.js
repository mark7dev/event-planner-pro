import React from 'react';
import Layout from '../components/layout';
import styled from '@emotion/styled';

const Section = styled.section`
    display: flex;
    align-items: center;
    flex-direction: column;
    padding: 40px;
    margin: 0 auto;
    width: 100%;
    height: 100vh;
    background-color: #FFFFFF;

    @media (max-width: 768px) {
        flex-direction: column;
        height: auto;
    }
`;

const Title = styled.h1`
    font-size: 3rem;
    text-align: center;
    margin: 2rem 0rem;
    color: #2B2D42;
`;

const Content = styled.p`
    font-size: 20px;
    line-height: 26px;
    text-align: center;
    margin-bottom: 40px;
    color: #8D99AE;
`;

const pricingInfo = [
    {
        name: 'Basic',
        price: '199',
        description: 'Per user, per month. Billed annually. The first step to explore our tool.',
    },
    {
        name: 'Pro',
        price: '299',
        description: 'Per user, per month. Billed annually. For people who are serious about managing events.',
    },
    {
        name: 'Business',
        price: '999',
        description: 'Per user, per month. Billed annually. For large organizations with many users.',
    }
]

const CardsContainers = styled.div`
    display: flex;
    justify-content: space-around;
    align-items: center;
    padding: 0 20px;
    margin: 80px 0px;
    width: 60%;

    @media (max-width: 768px) {
        flex-direction: column;
        height: auto;
    }
`;

const CardPrice = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 40px;
    margin: 20px;
    width: 300px;
    height: 320px;
    outline: 1px solid black;
    background-color: #F8F32B;
    border-radius: 10px;
    color: #2B2D42;

    h2 {
        font-size: 24px;
        font-weight: 600;
        line-height: 31px;
    }

    h3 {
        font-size: 42px;
        line-height: 52px;
    }

    p {
        font-size: 14px;
        line-height: 16px;
    }

    @media (max-width: 768px) {
        flex-direction: column;
        height: auto;
    }
`;

const Pricing = () => {
  return (
    <Layout>
        <Section>
            <Title>Pricing</Title>
            <Content>Our pricing is designed to fit your budget and needs.</Content>
            <CardsContainers>
                {pricingInfo.map((pricing, index) => (
                    <CardPrice key={index}>
                        <h2>{pricing.name}</h2>
                        <h3>${pricing.price}</h3>
                        <p>{pricing.description}</p>
                    </CardPrice>
                ))}
            </CardsContainers>
        </Section>
    </Layout>
  )
}

export const Head = () => (
    <>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Find the perfect pricing plan for your event management needs with our Basic, Pro, and Business plans. Sign up today and start managing your events like a pro!" />
        <meta name="keywords" content="event management, pricing plans, Basic plan, Pro plan, Business plan" />
        <meta name="author" content="mark7dev" />
        <title>Event Management Pricing Plans | EventPlanner Pro</title>
    </>
)

export default Pricing


