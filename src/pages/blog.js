import React from 'react';
import { useState, useEffect } from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../components/layout';
import CardBlog from '../components/cardBlog';
import BackgroundImage from 'gatsby-background-image';
import styled from '@emotion/styled';
import useBlogs from '../hooks/useBlogs';

const ImageBackground = styled(BackgroundImage)`
    // height: 700px;
    height: 100vh;
`;

const TextImage = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  color: #FFFFFF;
  text-align: center;
  background-color: rgba(0, 0, 0, 0.5);
  padding: 1rem;
`;

const CardsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin: 2rem 0;
`;

const Pagination = styled.div`
  display: flex;
  justify-content: center;
  margin: 2rem 0;
`;

const PageNumber = styled.button`
  margin: 0 0.5rem;
  padding: 0.5rem 1rem;
  border: none;
  background-color: ${props => props.active ? '#F8F32B' : '#FFFFFF'};
  color: ${props => props.active ? '000000' : '8D99AE'};
  cursor: pointer;
`;

const MainText = styled.h1`
    font-size: 80px;
    font-weight: 700;
    line-height: 62px;
    margin-bottom: 40px;
    color: #FFFFFF;
`;

const DescriptionText = styled.p`
    font-size: 24px;
    line-height: 31px;
    margin-bottom: 40px;
    color: #FFFFFF;
`;

const Blog = () => {

    const blogsPerPage = 2;
    const blogs = useBlogs();

    const [currentPage, setCurrentPage] = useState(1);
    const [data, setData] = useState([]);

    const indexOfLastBlog = currentPage * blogsPerPage;
    const indexOfFirstBlog = indexOfLastBlog - blogsPerPage;
    const currentBlogs = blogs.slice(indexOfFirstBlog, indexOfLastBlog);

    const { image } = useStaticQuery(graphql`
    query {
        image: file(relativePath: { eq: "blog.png" } ) {
          childImageSharp {
            fluid(quality: 90, maxWidth: 1920) {
                ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    `);

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(blogs.length / blogsPerPage); i++) {
        pageNumbers.push(i);
    }

    useEffect(() => {
        setData(currentBlogs);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentPage]);

    return (
        <Layout>
            <ImageBackground tag="section" fluid={image.childImageSharp.fluid} fadeIn="soft">
                <TextImage>
                    <MainText>Blog</MainText>
                    <DescriptionText>Discover Insights and Tips from Event Experts</DescriptionText>
                </TextImage>
            </ImageBackground>
            <CardsContainer>
                {data.map(blog => (
                    <CardBlog
                        key={blog.id}
                        blog={blog}
                    />
                ))}
            </CardsContainer>
            <Pagination>
                {pageNumbers.map(number => (
                    <PageNumber
                        key={number}
                        active={number === currentPage}
                        onClick={() => setCurrentPage(number)}
                    >
                        {number}
                    </PageNumber>
                ))}
            </Pagination>
        </Layout>
    )
}

export const Head = () => (
    <>
        <title>Blog</title>
        <meta name="description" content="Discover insights and tips from event experts on our blog. Learn about the latest trends and best practices in event planning, and find out how our expert team of planners can help you achieve your goals." />
        <meta name="keywords" content="event planning, event management, event tips, event trends, event experts" />
        <meta name="author" content="mark7dev" />
        <meta property="og:title" content="Blog" />
        <meta property="og:description" content="Discover insights and tips from event experts on our blog. Learn about the latest trends and best practices in event planning, and find out how our expert team of planners can help you achieve your goals." />
        <meta property="og:image" content="https://www.example.com/blog.png" />
        <meta property="og:url" content="https://astounding-sprinkles-1b5be0.netlify.app/blog/" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="EventPlanner Pro" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="Blog" />
        <meta name="twitter:description" content="Discover insights and tips from event experts on our blog. Learn about the latest trends and best practices in event planning, and find out how our expert team of planners can help you achieve your goals." />
        <meta name="twitter:image" content="https://www.example.com/blog.png" />
    </>
)

export default Blog