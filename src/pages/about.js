import React from 'react';
import Layout from '../components/layout';
import { StaticImage } from "gatsby-plugin-image";
import Partner from '../components/partner';
import styled from '@emotion/styled';
import { css } from '@emotion/react';
import usePartners from '../hooks/usePartners';

const Section = styled.section`
    display: flex;
    align-items: center;
    flex-direction: column;
    padding: 40px;
    margin: 0 auto;
    width: 100%;
    height: 100vh;
    background-color: #FFFFFF;
    color: #000000;

    @media (max-width: 768px) {
        flex-direction: column;
        height: auto;
    }
`;

const Title = styled.h1`
    font-size: 48px;
    font-weight: 600;
    line-height: 62px;
    text-align: center;
    color: #2B2D42;
`;

const Content = styled.p`
    font-size: 20px;
    line-height: 26px;
    text-align: center;
    margin-bottom: 40px;
    color: #8D99AE;
`;

const About = () => {

  const partners = usePartners();

  return (
    <Layout>
        <Section>
            <Title>About Us</Title>
            <Content>Simplify event management with EventPlanner Pro. Join our community and experience the benefits of streamlined event organization today!</Content>
            <StaticImage
              src="../images/about1.png"
              alt="image1"
              placeholder="blurred"
              width={500}
              quality={100}
              formats={["auto", "webp", "avif"]}
            />
        </Section>
        <Section>
            <Title>Our Mission</Title>
            <Content>At EventPlanner Pro, we are committed to providing excellent services to event organizers and planners, helping them achieve successful and memorable events. We believe in making a positive impact on the event industry and our community. Our dedicated team strives to deliver the best possible support and innovation.</Content>
            <StaticImage
              src="../images/about2.png"
              alt="image1"
              placeholder="blurred"
              width={500}
              quality={100}
              formats={["auto", "webp", "avif"]}
            />
        </Section>
        <Section
          css={css`
            @media (max-width: 1700px) {
              flex-direction: column;
              height: auto;
            }
          `}
        >
            <Title>Meet Our</Title>
            <Content>Get to know the people behind EventPlanner Pro</Content>
            <div
              css={css`
                display: flex;
                flex-wrap: wrap;
                justify-content: space-around;
                align-items: center;
                padding: 0 40px;
                margin: 0 auto;
                @media (max-width: 768px) {
                    flex-direction: column;
                    height: auto;
                }
              `}
            >
              {partners.map(partner => (
                <Partner
                  key={partner.id}
                  partner={partner}
                />
              ))}
            </div>
        </Section>
    </Layout>
  )
}

export const Head = () => (
  <>
    <title>About Us</title>
    <meta name="description" content="Learn more about our company and our mission. Find out how we can help you achieve your goals with our expert team of planners." />
  </>
)

export default About;
