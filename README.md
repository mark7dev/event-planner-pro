# Project Name

This is a Gatsby project that uses npm and React to create a fast and responsive website. The project includes a home page, an about page, and a blog page with individual blog posts. The site features a consistent layout with a header and footer, and includes navigation links to all pages. The project is designed to be easy to use and customize, and includes detailed documentation to help developers get started.

## Tools

- Gatsby
- DatoCMS

## Installation

1. Install [Gatsby CLI](https://www.gatsbyjs.com/docs/reference/gatsby-cli/)

2. Clone the repository:

```shell
    git clone https://gitlab.com/mark7dev/event-planner-pro.git
```

3. Install the dependencies:

```shell
    npm install
```

## Usage

1. Start the development server:

```shell
    gatsby develop
```

2. Open the app in your browser

3. Build the project
```shell
    gatsby build
```

4. Create production server preview
```shell
    gatsby serve
```

## Files

A quick look at the top-level files and directories you'll see in a typical Gatsby project.

    .
    ├── node_modules
    ├── src
    ├── .gitignore
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package.json
    └── README.md

### `gatsby-config.js`

This file contains the configuration settings for the Gatsby site, including metadata, plugins, and site structure.

### `src/components/header.js`

This file contains the Header component, which displays the site header and navigation menu.

### `src/components/layout.js`

This file contains the Layout component, which provides a consistent layout for all pages on the site.

### `src/components/footer.js`

This file contains the Footer component, which displays the site footer and copyright information.

### `src/pages/index.js`

This file contains the Index page component, which displays the home page of the site.

### `src/pages/about.js`

This file contains the About page component, which provides information about the company and its mission.

### `src/pages/blog.js`

This file contains the Blog page component, which displays a list of blog posts and allows users to read individual posts.

### `src/templates/blog-post.js`

This file contains the BlogPost template component, which displays the text content of a single blog post.

## Contributing

1. Fork the repository

2. Create a new branch

3. Make changes and commit them

4. Push to the branch

5. Create a new Pull Request

## Production url

https://astounding-sprinkles-1b5be0.netlify.app/

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.