/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/reference/config-files/gatsby-node/
 */

/**
 * @type {import('gatsby').GatsbyNode['createPages']}
 */
exports.createPages = async ({ actions, graphql, reporter }) => {
  const result = await graphql(`
    query {
      allDatoCmsBlog {
        nodes {
          slug
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panic("There was an error, there are no results", result.errors)
  }

  const blogs = result.data.allDatoCmsBlog.nodes;

  blogs.forEach(blog => {
    actions.createPage({
      path: `blog/${blog.slug}`,
      component: require.resolve("./src/templates/blogtext.js"),
      context: {
        slug: blog.slug
      }
    })
  })

  // const { createPage } = actions
  // createPage({
  //   path: "/using-dsg",
  //   component: require.resolve("./src/templates/using-dsg.js"),
  //   context: {},
  //   defer: true,
  // })
}
